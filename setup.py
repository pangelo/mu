#! /usr/bin/env python3

from setuptools import setup, find_packages

setup(name="mu",
      version="0.1.2",
      description="minimalist static site generator",
      author="Pedro Ângelo",
      author_email="pangelo@void.io",
      url="https://gitlab.com/pangelo/mu",
      license='GPL-3.0+',
      package_dir={'':'src'},
      packages=['mu','mu.plugin'],
      install_requires=['commonmark>=0.9.1', 'Jinja2>=2.10.1', 'PyYAML>=5.1.2'],
      entry_points={
          'console_scripts': ['mu=mu.__main__']
      }
)
