title: "Home"
---

# mu

A minimal static site generator

## Overview

mu is a small batch processor written in Python that by itself does not do much except loading a job description and loading a set of plugins

It builds a list of files from a source directory and sends it down a
a chain of plugins that do all the heavy lifting. 

It is aimed at being minimal, easy to understand and easy to customize for your purposes.

<!--
TODO:
 * more info
 * similar in spirit to gulp and metallist
-->

## Installation

mu is still not ready for widespread usage and is likely to change and
break a lot as development proceeds. Instead of installing the package
on your system, we recommend you use a Python3 virtualenv instead:

    python -m virtualenv -p python3 ./mu_dev
    cd ./mu_dev
    . ./bin/activate

For development:

    git clone https://gitlab.com/pangelo/mu.git ./src
    pip install -r ./src/requirements.txt
    pip install -e ./src

To build this documentation do:

    cd ./doc
    mu build

While it is not available at PYPI, if you just want to install the package for testing you can use:

    pip install https://gitlab.com/pangelo/mu/repository/archive.zip?ref=master

## Usage Example

TODO: a small static site

## Core Plugins

TODO: write a plugin to grab this data from the actual source files

## Writing New Plugins

A mu plugin is a Python object that provides a specific interface:

 * it can be instantiated and parametrised
 * it can be called to operate on a context object

A `Null` plugin, which you can use as a template for your own plugins is provided in the set of core plugins and looks like:

    from mu.plugin import Plugin
    
    class NullPlugin(Plugin):
    
        def __init__(self, options):
            self.name = "null"
        
        def __call__(self, context):
            return True
    
    def create(options):
        return NullPlugin(options)

an example of a more useful plugin is the `Counter` plugin which increments a value in the context whenever it is called:

    from mu.plugin import Plugin
    
    class CounterPlugin(Plugin):
    
        def __init__(self, options):
            self.name = "counter"
        
        def __call__(self, context):
            counter = context['meta'].get('counter', 0)
            counter = counter + 1
            context['meta']['counter'] = counter
            return True
    
    def create(options):
        return CounterPlugin(options)

TODO:
 * explain
   * the pipeline setup process
   * anatomy of the context object
   * error/status logging
   * how to do X?
