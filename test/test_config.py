import mu

def test_missing_config():
    config = mu.load_config ('./nonexistant')
    assert(config == None)

def test_default_config():
    config = mu.load_config (None)
    assert(config['source'] == './src')
    assert(config['destination'] == './build')
    assert(config['meta'] == {})
    assert(config['build'] == [])

def test_default_config_file():
    config = mu.load_config ('./data/config/default')
    assert(config['source'] == './src')
    assert(config['destination'] == './build')
    assert(config['meta'] == {})
    assert(config['build'] == [])

def test_example_config_file():
    config = mu.load_config('./data/config/example')
    assert(config['source'] == './src')
    assert(config['destination'] == './build')
    assert(config['meta']['author'])
    assert(len(config['build']) == 8)
    assert(len(config['build'][0].keys()) == 1)
    assert(list(config['build'][0].keys())[0] == 'ignore')
    assert(config['build'][4]['template']['source'] == './templates')
    assert(config['build'][5]['index']['/']['recursive'] == False)
