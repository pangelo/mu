import mu
import os, os.path

def test_build_config_file_missing(tmpdir):
    path = str(tmpdir)
    assert(not mu.build(path))

def test_build_valid_context(tmpdir):
    path = str(tmpdir)
    mu.init(path)
    config = mu.load_config(path)
    context = mu.create_context(path, config)
    assert(context)
    assert('path' in context)
    assert('meta' in context)
    assert('files' in context)

def test_build_context_invalid_src(tmpdir):
    context = mu.create_context(str(tmpdir), mu.default_config)
    assert(context == {})

def test_build_context_invalid_dst(tmpdir):
    path = str(tmpdir)
    config = mu.default_config
    os.mkdir(os.path.join(path, config['source']))
    context = mu.create_context(path, config)
    assert(os.path.exists(context['path']['destination']))

def test_build_context_file_list():
    config = mu.load_config('./data/build/context')
    context = mu.create_context('./data/build/context', config)
    assert(len(context['files']) == 3)
    for f in context['files']:
        assert(f['path'] in ['./data/build/context/./src/a',
                             './data/build/context/./src/b',
                             './data/build/context/./src/c/d'])

def test_build_empty_pipeline():
    config = mu.load_config('./data/build/context')
    context = mu.create_context('./data/build/context', config)
    pipeline = mu.create_pipeline(config, context)
    assert(pipeline == [])

def test_run_empty_pipeline():
    config = mu.load_config('./data/build/context')
    context = mu.create_context('./data/build/context', config)
    pipeline = mu.create_pipeline(config, context)
    status = mu.run_pipeline(pipeline, context)
    assert(status)


