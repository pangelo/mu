#
# Reusable test fixtures and helper functions
#

import mu
import mu.plugin
import os.path
import shutil

def build_and_run_pipeline(path):
    "build and run a mu pipeline from a specific data dir"
    config = mu.load_config(path)
    context = mu.create_context(path, config)
    pipeline = mu.create_pipeline(config, context)
    status = mu.run_pipeline(pipeline, context)
    return status, context
