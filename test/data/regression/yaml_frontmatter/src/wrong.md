title: "wrong separator"

----

Sometimes the user inputs the wrong number of dashes in the metadata
separator. In this case, all the text should be passed on to the
content section.

