from mu.plugin import Plugin

class LoadedPlugin(Plugin):
    "A simple mu plugin that asserts that it has loaded"

    def __init__(self, options):
        self.name = "loaded"
        self.tag = self.name
        if options:
          self.tag = options.get('tag', self.name)

    def __call__(self, context):
        context['meta'][self.tag] = True
        return True

def create(options):
    return LoadedPlugin(options)
