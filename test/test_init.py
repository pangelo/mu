import subprocess, os, os.path

def test_init (tmpdir):
    "check that init command works on an empty dir"
    status = subprocess.call (['mu', 'init', str(tmpdir)])
    assert (status == 0)

def test_init_existing_src(tmpdir):
    "check that init command works with existing source dir"
    os.mkdir(str(tmpdir) + '/src')
    status = subprocess.call (['mu', 'init', str(tmpdir)])
    assert(status == 0)

def test_init_existing_dst(tmpdir):
    "check that init command removes any existing destination dir"
    dst_path = str(tmpdir) + '/build'
    test_file_path = dst_path + '/test.txt' 
    os.mkdir(dst_path)
    open(test_file_path, 'w').close()
    status = subprocess.call (['mu', 'init', str(tmpdir)])
    assert(status == 0)
    try:
        fstatus = os.stat(test_file_path)
    except FileNotFoundError:
        assert(True)
    else:
        assert(False)
