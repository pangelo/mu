import subprocess

def test_onearg():
    status = subprocess.call ('mu')
    assert (status == 1)

def test_manyarg():
    status = subprocess.call(['mu', 'init', 'blah', 'blah'])
    assert (status == 1)
    
def test_badarg():
    status = subprocess.call (['mu', 'blah'])
    assert (status == 1)
