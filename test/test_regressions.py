import mu
import mu.plugin
import os.path
import shutil

from fixtures import build_and_run_pipeline

def test_yaml_frontmatter():
    "#1: parsing YAML frontmatter can break"
    status, context = build_and_run_pipeline('./data/regression/yaml_frontmatter')
    assert(status)
    for item in context['files']:
      assert(item['meta'] == {})
