import mu
import mu.plugin
import os.path
import shutil

from fixtures import build_and_run_pipeline

def test_load_from_config():
    status, context = build_and_run_pipeline('./data/plugin/load')
    assert(status)
    assert(context['meta']['loaded'] == True)

def test_null_plugin():
    assert(mu.build('./data/plugin/null'))

def test_counter_plugin():
    status, context = build_and_run_pipeline('./data/plugin/counter')
    assert(status)
    assert('counter' in context['meta'])
    assert(context['meta']['counter'] == 3)

def test_filter_entries():
    status, context = build_and_run_pipeline('./data/plugin/filter_entries')
    mds = mu.plugin.filter_entries(context, '*.md')
    txts = mu.plugin.filter_entries(context, '*.txt')
    assert(status)
    assert(len(mds) == 2)
    assert(txts == [])

def test_ignore_plugin():
    status, context = build_and_run_pipeline('./data/plugin/ignore')
    assert(status)
    txts = [f['path'] for f in context['files'] if f['path'].endswith('.txt')]
    mds = [f['path'] for f in context['files'] if f['path'].endswith('.md')]
    rels = [f['path'] for f in context['files'] if f['path'].endswith('.rel')]
    assert(txts == [])
    assert(rels == [])
    assert(len(mds) == 2)


def test_static_plugin():
    status, context = build_and_run_pipeline('./data/plugin/static')
    assert(status)
    assert(context['meta']['static'] == 'static')
    static_path = os.path.join(context['path']['destination'], context['meta']['static'])
    assert(os.path.exists(static_path))
    assert(os.path.exists(os.path.join(static_path, 'static.txt')))
    shutil.rmtree(static_path)


def test_markdown_plugin():
    status, context = build_and_run_pipeline('./data/plugin/markdown')
    assert(status)
    destination = context['path']['destination']
    md = [f for f in context['files'] if f['path'].endswith('.html')][0]
    assert(not os.path.exists(os.path.join(destination, 'b.html')))
    assert(os.path.exists(md['path']))
    assert('test' in md['meta'])
    assert(md['meta']['test'] == 'passed')
    os.remove(md['path'])
    assert(os.path.exists(os.path.join(destination, 'c/d.html')))
    assert(os.path.exists(os.path.join(destination, 'c/e.html')))
    assert(os.path.exists(os.path.join(destination, 'c/f/g.html')))
    shutil.rmtree(os.path.join(destination, 'c'))

def test_template_plugin():
    status, context = build_and_run_pipeline("./data/plugin/template")
    assert(status)
    default_test_path = "./data/plugin/template/build/default.html"
    with open(default_test_path, 'r') as default_test_file:
        assert("<title>default</title>" in default_test_file.read())
    os.remove(default_test_path)
    override_test_path = './data/plugin/template/build/override.html'
    with open(override_test_path, 'r') as override_test_file:
        assert("<title>override</title>" in override_test_file.read())
    os.remove(override_test_path)
    ignored_test_path = './data/plugin/template/src/ignored.txt'
    with open(ignored_test_path, 'r') as ignored_test_file:
        assert("test_ok" == ignored_test_file.read())
    overwrite_test_path = './data/plugin/template/build/overwrite.html'
    with open(overwrite_test_path, 'r') as overwrite_test_file:
        assert("test_ok" == overwrite_test_file.read())
    static_test_path = './data/plugin/template/build/d/static.html'
    with open(static_test_path, 'r') as static_test_file:
        assert("../static/test.txt" == static_test_file.read())
