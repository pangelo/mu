import sys
import mu

def print_usage():
    txt="""

    mu is a minimalistic static site generator

    Usage: 
    
      mu <command> [arguments]
    
    Where COMMAND is one of:
      
      init [path]     start a new site in PATH
      build [path]    render the site defined in PATH
      post [path]     start a new blog post in the blog defined in PATH
      serve [path]    start a simple web server on PATH
      help            print this help info
    """
    print(txt)

if len(sys.argv) == 1 or len(sys.argv) > 3:
    print_usage()
    sys.exit(1)

command = sys.argv[1]
path = '.'

if len(sys.argv) == 3:
    path = sys.argv[2]

if command == 'init':
    mu.init(path)
elif command == 'build':
    mu.build(path)
elif command == 'post':
    mu.post(path)
elif command == 'serve':
    mu.serve(path)
elif command == 'help':
    print_usage()
else:
    print("mu command '{}' was not recognized".format(command))
    sys.exit(1)

sys.exit(0)
