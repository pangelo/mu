# null plugin for mu, doesn't do anything

from mu.plugin import Plugin

class NullPlugin(Plugin):

    def __init__(self, options):
        self.name = "null"
    
    def __call__(self, context):
        return True

def create(options):
    return NullPlugin(options)
