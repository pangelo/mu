from fnmatch import fnmatch
import os.path
import io
import commonmark
import yaml

import mu.plugin


class MarkdownPlugin(mu.plugin.Plugin):
    """
    # mu markdown plugin

    Converts a series of markdown text files from source path to html
    files in the destination path. 

    If the file begins with a YAML block terminated by `---` in a
    single line, the block is parsed and added to the file entry's
    metadata.

    # Options

    * filter: a glob expression to select file entries for processing
              (default: "*.md")

    # Example build stanza

        - markdown
            filter: "*.md"

    """

    def __init__(self, options):
        self.name = "markdown"
        options = options or {}
        self.filter = options.get('filter', "*.md")

    def __call__(self, context):
        entries = mu.plugin.filter_entries(context, self.filter)
        parser = commonmark.Parser()
        renderer = commonmark.HtmlRenderer()
        for entry in entries:
            with open(entry['path'], 'r') as input_file:
                meta, mdown = self.extract_meta(input_file)
            if meta:
                entry['meta'].update(meta)
            ast = parser.parse(mdown)
            rpath = os.path.relpath(entry['path'], context['path']['source'])
            dirname, fname = os.path.split(rpath)
            root, ext = os.path.splitext(fname)
            output_dir = os.path.join(context['path']['destination'], dirname)
            if not os.path.exists(output_dir):
                os.makedirs(output_dir)
            output_path = os.path.join(output_dir, root + '.html')
            with open(output_path, 'w') as output_file:
                output_file.write(renderer.render(ast))
            entry['path'] = output_path
        return True

    def extract_meta(self, stream):
        """
        Extract and parse the YAML metadata block from the file stream if it exists.
        """        
        buf = io.StringIO()
        meta = {}
        line = stream.readline()
        while (line):
            if line.rstrip() == '---':
                meta = yaml.safe_load(buf.getvalue())
                buf = io.StringIO()
                buf.write(stream.read())
                break
            buf.write(line)
            line = stream.readline()
        return meta, buf.getvalue()

    
def create(options):
    return MarkdownPlugin(options)
