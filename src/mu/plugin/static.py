import os.path, shutil

from mu.plugin import Plugin


class StaticFilesPlugin(Plugin):

    """ 
    # mu static files plugin

    Copies a file tree from a source path to a specified path in the
    build dir and sets a meta key accordingly

    ## Options

    * source: parent folder of the file tree to copy
              (default: "./static")

    * destination: relative path of a folder inside the build dir to
                   copy the file tree into. The folder will be created
                   if it doesn't exist. (default: "static")

    ## Example build stanza

        - static
            source: "static"
            destination: "static"

    """
    
    def __init__(self, options):
        self.name = "static"
        options = options or {}
        self.source = options.get('source', './static')
        self.destination = options.get('destination', 'static')

    def __call__(self, context):
        src_path = os.path.join(context['path']['base'], self.source)
        if not os.path.exists(src_path):
            return False
        dst_path = os.path.join(context['path']['destination'], self.destination)
        if os.path.exists(dst_path):
            shutil.rmtree(dst_path)
        shutil.copytree(src_path, dst_path)
        context['meta']['static'] = self.destination
        return True


def create(options):
    return StaticFilesPlugin(options)
