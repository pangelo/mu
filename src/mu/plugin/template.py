import jinja2
import os.path

import mu.plugin

class TemplatePlugin(mu.plugin.Plugin):
    """# mu template plugin

    Runs a set of file entries through the Jinja2 template engine if a
    "template" meta entry with the name of the template to use exists. 
    
    Per file "template" meta entries override global entries, this
    allows you to set a global template to use in the meta section of
    your config.yaml and override that setting on a per-file basis.

    Global and file-specific meta entries are merged to build the
    template's render context. Inside a template, the {{ content }}
    tag is replaced with the file's contents.

    # Options
    
    * source: the base path from where the templates are loaded
              (default: "./templates")

    * filter: a glob expression to select file entries for processing
              (default: "*.html")
    
    # Example build stanza

        template: "default.html"

        - template:
            source: "./templates"
            filter: "*"

    """

    def __init__(self, options):
        self.name = "template"
        options = options or {}
        self.source = options.get("source", "./templates")
        self.filter = options.get("filter", "*.html")

    def __call__(self, context):
        entries = mu.plugin.filter_entries(context, self.filter)
        template_path = os.path.join(context['path']['base'], self.source)
        engine = jinja2.Environment(loader=jinja2.FileSystemLoader(template_path))
        engine.globals = context['meta']
        for entry in entries:
            template_name = entry['meta'].get("template", context['meta'].get("template", ""))
            if not template_name:
                continue
            template = engine.get_template(template_name)
            static_path = os.path.join(context['path']['destination'], context['meta']['static'])
            rel_static_path = os.path.relpath(static_path, os.path.dirname(entry['path']))
            entry['meta']['static'] = rel_static_path
            template_context = dict(entry['meta'])
            with open(entry['path'], 'r+') as work_file:
                template_context['content'] = work_file.read()
                work_file.seek(0)
                work_file.truncate()
                work_file.write(template.render(template_context))
        return True

    
def create(options):
    return TemplatePlugin(options)
