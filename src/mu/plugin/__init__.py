import importlib as imp
import os.path
from fnmatch import fnmatch


class Plugin(object):
    """
    Base class for mu plugins
    """
    
    def __init__(self, options = {}):
        self.name = "base"
        pass

    def __call__(self, context):
        return False

def load(plugin_data, config, context):
    plugin_name = list(plugin_data.keys())[0]
    opts = plugin_data[plugin_name]
    plugin_spec = imp.util.find_spec('mu.plugin.' + plugin_name)
    if not plugin_spec:
        for path in config.get('plugins', []):
            base_path = os.path.join(context['path']['base'], path)
            plugin_path = os.path.join (base_path, plugin_name + '.py')
            plugin_spec = imp.util.spec_from_file_location(plugin_name, plugin_path)
            if plugin_spec:
                break
    if not plugin_spec:
        return None
    plugin = imp.util.module_from_spec(plugin_spec)
    plugin_spec.loader.exec_module(plugin)
    return plugin.create(opts)


def filter_entries(context, pattern):
    "Plugin helper to filter a set of file entries using a glob pattern"
    return [e for e in context['files'] if fnmatch(e['path'], pattern)]
