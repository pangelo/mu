# mu ignore plugin - filter out a set of file patterns from the context

import os.path
from fnmatch import fnmatch

from mu.plugin import Plugin


class IgnorePlugin(Plugin):
    """
    # mu ignore files plugin

    Filters out file entries from the context based on a set of glob
    patterns.

    ## Options

    Accepts a single option, a list of glob patterns to ignore
    (defaults to ['.*', '*~'])

    ## Example build stanza

        - ignore: ["drafts/*", "*~"]

    """
    
    def __init__(self, options):
        self.name = "ignore"
        self.patterns = options or ['.*', '*~']

    def __call__(self, context):
        matches = []
        for f in context['files']:
            fpath = os.path.relpath(f['path'], context['path']['source'])
            for pat in self.patterns:
                if fnmatch(fpath, pat):
                    matches.append(f)
                    break
        for m in matches:
            context['files'].remove(m)
        return True


def create(options):
    return IgnorePlugin(options)
