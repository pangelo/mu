# mu counter plugin - increments a counter in the context

from mu.plugin import Plugin

class CounterPlugin(Plugin):

    def __init__(self, options):
        self.name = "counter"
    
    def __call__(self, context):
        counter = context['meta'].get('counter', 0)
        counter = counter + 1
        context['meta']['counter'] = counter
        return True

def create(options):
    return CounterPlugin(options)

