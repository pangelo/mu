import os, os.path, shutil
import yaml
import mu.plugin

default_config = {
    'source': './src',
    'destination': './build',
    'meta': {},
    'build': []
}


def init(path):
    print("mu init:", path)
    if not os.path.exists(path):
        os.mkdir(path)
    config = load_config(None)
    write_config (path, config)
    src_path = os.path.join(path, config['source'])
    if not os.path.exists(src_path):
        os.mkdir(src_path)
    dst_path = os.path.join(path, config['destination'])
    if os.path.exists(dst_path):
        shutil.rmtree(dst_path)
    os.mkdir(dst_path)
    return True


def build(path):
    print("mu build:", path)
    config = load_config(path)
    if not config:
        return False
    context = create_context(path, config)
    if not context:
        return False
    pipeline = create_pipeline(config, context)
    if not pipeline:
        return False
    return run_pipeline(pipeline, context)


def load_config(path):
    if not path:
        return default_config
    config_path = path + '/config.yaml'
    if not os.path.exists(config_path):
        print("mu config file '{}' not found".format(config_path))
        return None
    with open(config_path,'r') as config_file:
        config = yaml.load(config_file)
    return config


def write_config (path, config):
    if not path or not os.path.exists(path):
        return
    config_path = path + '/config.yaml'
    with open(config_path, 'w') as config_file:
        yaml.dump(config, config_file)


def create_context(path, config):
    context = {
        'path': {
            'base': path,
            'source': os.path.join(path, config.get('source', './src')),
            'destination': os.path.join(path, config.get('destination', './build'))
        },
        'meta': dict(config.get('meta', {})),
        'files': []
    }

    if not os.path.exists(context['path']['source']):
        return {}
    if not os.path.exists(context['path']['destination']):
        os.mkdir(context['path']['destination'])
    for root, dirs, files in os.walk(context['path']['source']):
        for f in files:
            file_entry = {
                'path': os.path.join(root, f),
                'meta': {}
            }
            context['files'].append(file_entry)
    return context


def create_pipeline(config, context):
    pipeline = []
    for step in config.get('build',[]):
        if type(step) is str:
            step = { step: {} }
        plugin = mu.plugin.load(step, config, context)
        if not plugin:
            print ("mu failed to load plugin {}".format(step))
            return []
        pipeline.append(plugin)
    return pipeline


def run_pipeline(pipeline, context):
    print ("mu running pipeline")
    for process in pipeline:
        print("  ", process.name)
        status = process(context)
        if not status:
            return False
    print ("done")
    return True

def serve(path):
    print ("mu serve:", path,  "port 8000")
    config = load_config(path)
    if not config:
        return False
    context = create_context(path, config)
    if not context:
        return False

    from http.server import HTTPServer, SimpleHTTPRequestHandler

    base_dir = os.getcwd()
    os.chdir(context['path']['destination'])
    httpd = HTTPServer(('',8000), SimpleHTTPRequestHandler)
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        print ("mu server terminated")
    os.chdir(base_dir)
    return True
